package syncSound.server;

import syncSound.network.TCPConnection;
import syncSound.network.TCPConnectionListener;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

/**
 * Created by dmitriy on 21.10.17.
 */
public class SyncSoundServer implements TCPConnectionListener{

    public static void main(String[] args) {
        new SyncSoundServer();
    }

    private final ArrayList<TCPConnection> connections = new ArrayList<>();

    private SyncSoundServer() {
        System.out.println("Server running.");
        try (ServerSocket serverSocket = new ServerSocket(4546)) {
            while (true) {
                try {
                    new TCPConnection(this, serverSocket.accept());
                } catch (IOException e) {
                    System.out.println("TCPConnection exception: " + e);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public synchronized void onConnectionReady(TCPConnection tcpConnection) {
        connections.add(tcpConnection);
        sendToAllConnections("Client connected: " + tcpConnection);
    }

    @Override
    public synchronized void onReceiveString(TCPConnection tcpConnection, String value) {
        if (value == null) {
            tcpConnection.disconnect();
            return;
        }
        sendToAllConnections(value);
    }

    @Override
    public synchronized void onDisconnect(TCPConnection tcpConnection) {
        connections.remove(tcpConnection);
        sendToAllConnections("Client disconnected: " + tcpConnection);
    }

    @Override
    public synchronized void onException(TCPConnection tcpConnection, Exception e) {
        System.out.println("TCPConnection exception: " + e);
    }

    private void sendToAllConnections(String value) {
        System.out.println(value);
        final int sizeOfCnt = connections.size();
        for (int i = 0; i < sizeOfCnt; i++) {
            connections.get(i).sendString(value);
        }
    }
}
